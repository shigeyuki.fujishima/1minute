GOMOD_CONF := GO111MODULE=on
GO := $(GOMOD_CONF) go
REPOSITORY := gitlab.com/shigeyuki.fujishima
APP_NAME := 1minute

GOARCH := amd64
GOOS := $(shell uname | tr '[:upper:]' '[:lower:]')

init:
	mkdir .tool
	wget ''
	$(GO) mod $(REPOSITORY)/$(APP_NAME)

go_get:
	$(GO) get -u $(ARG)

go_vendor:
	$(GO) mod vendor

run_server:
	go run main.go --config $(PWD)/config.yaml server

build:
	mkdir -pv bin
	GOOS=$(GOOS) GOARCH=$(GOARCH) go build -ldflags '-extldflags "-static" -w -s' -o bin/$(APP_NAME)_$(GOOS)_$(GOARCH)

clean:
	$(RM) -rfv bin
