/*
Copyright © 2019 Shigeyuki Fujishima <shigeyuki.fujishima@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"os"
	"strings"

	"github.com/mattn/go-pipeline"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	flgCommand string
)

func run(cmd *cobra.Command, args []string) error {
	configCommands := viper.GetStringMap("commands")

	// var ok bool
	commands, ok := configCommands[flgCommand]
	if !ok {
		return errors.Errorf("invalid command: %s. Allowed command keys: %v", flgCommand, MapKeys(configCommands))
	}

	commandsTobeRun := make([][]string, len(commands.([]interface{})))
	for i, command := range commands.([]interface{}) {
		commandsTobeRun[i] = strings.Split(command.(string), " ")
	}

	if out, err := pipeline.Output(commandsTobeRun...); err != nil {
		cmd.SilenceUsage = true
		cmd.SilenceErrors = true
		return errors.Wrapf(err, "failed to execute command %s", flgCommand)
	} else {
		cmd.Println(string(out))
	}

	return nil
}

// commandCmd represents the command command
var commandCmd = &cobra.Command{
	Use:   "command",
	Short: "Run given command",
	Long:  `Rin given command`,
	RunE:  run,
}

func init() {
	rootCmd.AddCommand(commandCmd)

	commandCmd.Flags().StringVarP(&flgCommand, "command", "c", "", "command name you want to run(required)")

	if err := commandCmd.MarkFlagRequired("command"); err != nil {
		fmt.Println(errors.Wrap(err, "failed to mark flags as required"))
		os.Exit(1)
	}
}
