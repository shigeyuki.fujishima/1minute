/*
Copyright © 2019 Shigeyuki Fujishima <shigeyuki.fujishima@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"os/exec"
	"strings"
	"time"

	"go.uber.org/zap/zapcore"

	"go.uber.org/zap"

	pipeline "github.com/mattn/go-pipeline"
	"github.com/pkg/errors"
	"github.com/savsgio/atreugo"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	serverAddress string
	port          int
)

const (
	defaultServer = "127.0.0.1"
	defaultPort   = 8000
)

func execute(commands [][]string) ([]byte, error) {
	switch {
	case len(commands) == 1:
		var stdout bytes.Buffer
		var stderr bytes.Buffer
		var c *exec.Cmd

		command := commands[0]
		if len(command) == 1 {
			c = exec.Command(command[0])
		} else {
			c = exec.Command(command[0], command[1:]...)
		}
		c.Stdout = &stdout
		c.Stderr = &stderr

		err := c.Run()
		if err != nil {
			return nil, errors.Wrap(err, stderr.String())
		}

		return stdout.Bytes(), nil
	case len(commands) > 1:
		return pipeline.Output(commands...)
	default:
		return nil, errors.New("passed command is empty string")
	}
}

func errResponse(ctx *atreugo.RequestCtx, response map[string]interface{}, err error, code int) error {
	response["error"] = true
	response["message"] = err.Error()
	response["responsed_at"] = time.Now().Unix()
	return ctx.JSONResponse(response, http.StatusInternalServerError)
}

func IndexHandler(ctx *atreugo.RequestCtx) error {
	return ctx.RawResponseBytes(nil, http.StatusOK)
}

func RunHandler(ctx *atreugo.RequestCtx) error {
	command := string(ctx.QueryArgs().Peek("command"))
	requestPath := string(ctx.Path())
	response := atreugo.JSON{"command": command, "error": false, "request_uri": requestPath}
	fields := []zapcore.Field{
		zap.String("request_uri", requestPath),
		zap.String("command", response["command"].(string)),
	}

	predefinedCommands := viper.GetStringMap("commands")
	commands, ok := predefinedCommands[command]
	if !ok {
		err := errors.Errorf("invalid command: %s. Allowed command keys: %v", flgCommand, MapKeys(predefinedCommands))
		fields = append(fields, zap.String("status", http.StatusText(http.StatusBadRequest)))
		zap.L().Error(err.Error(), fields...)
		return errResponse(ctx, response, err, http.StatusBadRequest)
	}

	commandsTobeRun := make([][]string, len(commands.([]interface{})))
	for i, command := range commands.([]interface{}) {
		commandsTobeRun[i] = strings.Split(command.(string), " ")
	}

	out, err := execute(commandsTobeRun)
	if err != nil {
		err = errors.Wrapf(err, "failed to execute command %s", commandsTobeRun)
		fields = append(fields, zap.String("status", http.StatusText(http.StatusInternalServerError)))
		zap.L().Error(err.Error(), fields...)
		return errResponse(ctx, response, err, http.StatusInternalServerError)
	}

	response["message"] = string(bytes.TrimSpace(out))
	response["responsed_at"] = time.Now().Unix()

	fields = append(fields, zap.String("status", http.StatusText(http.StatusOK)))
	zap.L().Info(response["message"].(string), fields...)

	return ctx.JSONResponse(response, http.StatusOK)
}

func server(cmd *cobra.Command, args []string) error {
	serverConfig := &atreugo.Config{
		Host: serverAddress,
		Port: port,
	}

	webServer := atreugo.New(serverConfig)
	webServer.SetLogOutput(ioutil.Discard)
	webServer.Path("GET", "/", IndexHandler)
	webServer.Path("GET", "/run", RunHandler)

	if err := webServer.ListenAndServe(); err != nil {
		cmd.SilenceUsage = true
		cmd.SilenceErrors = true
		return err
	}
	return nil
}

// serverCmd represents the server command
var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "Up server",
	Long:  `Up server`,
	RunE:  server,
}

func init() {
	rootCmd.AddCommand(serverCmd)

	serverCmd.Flags().StringVarP(&serverAddress, "server ", "s", defaultServer, "Bind server address. default: "+defaultServer)
	serverCmd.Flags().IntVarP(&port, "port", "p", defaultPort, fmt.Sprintf("Bind server port. default: %d", defaultPort))
}
