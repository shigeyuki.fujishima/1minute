/*
Copyright © 2019 Shigeyuki Fujishima <shigeyuki.fujishima@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"os"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var (
	cfgFile   string
	verbose   bool
	debug     bool
	zapLogger *zap.Logger
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "1minite",
	Short: "root command",
	Long:  `root command`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	var exitStatus int
	if err := rootCmd.Execute(); err != nil {
		rootCmd.PrintErr(err)
		exitStatus = 1
	}

	if err := zapLogger.Sync(); err != nil {
		exitStatus++
	}

	if exitStatus > 0 {
		os.Exit(exitStatus)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $CWD/.1minite.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolVarP(&verbose, "verbose", "v", false, "Verbose message (for production use)")
	rootCmd.Flags().BoolVarP(&debug, "debug", "d", false, "Output log message with debug level (for development use)")

	cobra.OnInitialize(func() {
		// TODO: Refacotr to make these configuration utility function
		zapConfig := zap.NewProductionConfig()
		zapConfig.DisableStacktrace = true
		switch {
		case debug:
			zapConfig.Level = zap.NewAtomicLevelAt(zapcore.DebugLevel)
		case verbose:
			zapConfig.Level = zap.NewAtomicLevelAt(zapcore.InfoLevel)
		}
		var err error
		zapLogger, err = zap.Config.Build(zapConfig)
		if err != nil {
			rootCmd.PrintErrf("failed to initializa logger: %v", err)
			os.Exit(1)
		}
		zap.ReplaceGlobals(zapLogger)
	})
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find current working directory.
		confDir, err := GetCWD()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in current working directory with name ".1minite" (without extension).
		viper.AddConfigPath(confDir)
		viper.SetConfigName("config")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err != nil {
		fmt.Println(errors.Wrap(err, "failed to read config file"))
		os.Exit(1)
	}
}
