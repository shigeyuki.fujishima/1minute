package cmd

import (
	"os"
	"path/filepath"
)

func GetCWD() (string, error) {
	e, err := os.Executable()
	if err != nil {
		return "", err
	}

	return filepath.Dir(e), nil
}

func MapKeys(m map[string]interface{}) []string {
	keys := make([]string, len(m))
	i := 0
	for k := range m {
		keys[i] = k
		i++
	}

	return keys
}
